<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//require_once dirname(__FILE__) . '/fpdf/fpdf.php';
require_once dirname(__FILE__) . '/fpdf/WriteHTML.php';

class Fpdf_master {

    public function __construct() {

        $pdf = new PDF_HTML();
        $pdf->AddPage();

        $CI = & get_instance();
        $CI->fpdf = $pdf;
    }

}
