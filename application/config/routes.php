<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Login
$route['autenticar'] = 'Login/autenticarLogin';
$route['logout'] = 'Login/logout';

//Sistema
$route['sistema/home'] = 'Sistema/index';

//Filiados
$route['filiados'] = 'usuario/telaFiliados';
$route['excluirFiliado/(:num)/(:num)'] = 'usuario/excluirFiliado/$1/$2';
$route['editarFiliado/(:num)'] = 'usuario/editarFiliado/$1';
$route['updateFiliado'] = 'usuario/updateFiliado';

//Formularios
$route['formulario/filiacao'] = 'Form/formularioFiliacao';
$route['formulario/filiacao/insert'] = 'Form/insertFiliado';
$route['formulario/convite'] = 'Form/formularioConvite';
$route['enviar/convite'] = 'Form/enviarConvite';
$route['email'] = 'Form/layoutEmail';

//Usuários do sistema
$route['usuarios/sistema'] = 'usuario/telaUsuarios';
$route['cadastro/usuario'] = 'usuario/cadastroUsuario';
$route['cadastrar/usuario'] = 'usuario/cadastrarUsuario';
$route['excluir/usuario/(:num)/(:num)'] = 'usuario/excluirUsuario/$1/$2';

//Boleto
$route['gerarboleto'] = 'Form/geraBoleto';
$route['gerarboleto/laravel'] = 'Laravel_boleto/index';
$route['infoboleto'] = 'usuario/telaInfoBoleto';
$route['getBoleto'] = 'Form/getBoleto';
$route['cadastro_boleto/(:num)/(:any)'] = 'Form/cadastrandoBoleto/$1/$2';
$route['geraremessa'] = 'Remessa/geraRemessa';

//Financeiro
$route['financeiro'] = 'Financeiro/index';
$route['remessa'] = 'Remessa/remessa';
$route['gerar_remessa'] = 'Remessa/gerarRemessa';
$route['retorno'] = 'Retorno/retorno';
$route['visualizar_retorno'] = 'Retorno/telaVisualizarRetorno';
$route['visualizar_arquivo_retorno'] = 'Retorno/visualizarRetorno';
$route['gerar_retorno'] = 'Retorno/gerarRetorno';
$route['boletos_baixa'] = 'Boleto/boletosComBaixa';

