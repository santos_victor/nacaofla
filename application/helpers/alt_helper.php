<?php

if (!function_exists('iniciar_sessao')) {

    function iniciar_sessao($usuario) {
        $ci = & get_instance();
        $ci->session->set_userdata($usuario);
    }

}
if (!function_exists('usuario')) {

    function usuario($field = '', $value = '', $clear = false) {

        $ci = & get_instance();

        if ($value != '' || $clear == true):
            $ci->session->set_userdata($field, $value);
        endif;

        if ($field != ''):
            return $ci->session->userdata($field);
        else:
            return $ci->session->all_userdata();
        endif;
    }

}

if (!function_exists('finalizar_sessao')) {

    function finalizar_sessao() {
        $ci = & get_instance();
        $ci->session->sess_destroy();
    }

}

if (!function_exists('logado')) {

    function logado() {
        $ci = & get_instance();
        $ok = $ci->session->userdata('usuario_logado');
        if (!empty($ok)):
            $ussu = usuario();
            if (!isset($ussu['usuario_id'])):
                finalizar_sessao();
                redirect('home');
            endif;
        endif;
        if (empty($ok)):
            redirect('login');
        endif;
    }

}

if (!function_exists('formataDate')):

    function formataDate($date, $separator, $hr='') {
		$hora = '';
        if ($date != '') {
            list($dia,$mes,$ano) = explode($separator, $date);
            if ($separator == "-"){
            	list($a,$h) = explode(' ',$ano);
				if(!empty($hr) && !empty($h)){
					$hora = $h;
				}
				$ano = $a;
				$separator02 = "/";
			} else{
				$separator02 = "-";
			}
            $dt = $ano . $separator02 . $mes . $separator02 . $dia;
			return (!empty($hr) && !empty($h))?"$dt $hora":$dt;
        }
    }

endif;


function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i = 0; $i < $file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

if (!function_exists('formataDateAll')):

        function formataDateAll($date, $separator) {
            if ($date != '') {
                $date = explode($separator, $date);
                if ($separator == "-")
                    $separator02 = "/";
                else
                    $separator02 = "-";
                $date2 = $date[2] . $separator02 . $date[1] . $separator02 . $date[0];
                return $date2;
            }
        }

    endif;
