<?php

//Upload de img com preview
if (!function_exists('upImg')) {

    function upImg($campo='imagem',$src='',$titulo='',$file='fl',$arquivo='') {
        echo " <label for='file'>$titulo</label>
                <input id='file' name='$file' type='file' class='form-control' /><br>
                <input id='img' type='hidden' name='$campo' value='$arquivo' />
                <center>
                    <img id='preview' src='$src' class='img-fluid' alt='Prévia da imagem' style='border-radius:10px; width: 150px;' />
                </center> ";
    }

}

//Upload de segunda img com preview
if (!function_exists('upImgSeg')) {

    function upImgSeg($campo='imagem',$src='',$titulo='',$file='fl',$arquivo='') {
        echo " <label for='file'>$titulo</label>
                <input id='file' name='$file' type='file' class='form-control' /><br>
                <input id='img' type='hidden' name='$campo' value='$arquivo' />
                <center>
                    <img id='preview' src='$src' class='img-fluid' alt='Prévia da imagem' style='border-radius:10px; width: 150px;' />
                </center> ";
    }

}


if(!function_exists('getMenu')){
	function getMenu(){
		$ci = & get_instance();
		$ci->load->model('Geral_model','geral');
		$options['joins'][] = $ci->geral->gJoin('tb_paginas','pag_id','tb_menu','paginas_id');
		$options['joins'][] = $ci->geral->gJoin('tb_formularios','for_id','tb_menu','formularios_id');
		return $ci->geral->select('tb_menu',$options);
	}
}

if(!function_exists('getCategoria')){
	function getCategoria(){
		$ci = & get_instance();
		$ci->load->model('Geral_model','geral');
		return $ci->geral->select('tb_categorias');
	}
}

if(!function_exists('getSubCategoria')){
	function getSubCategoria($id){
		$ci = & get_instance();
		$ci->load->model('Geral_model','geral');
		$options['wheres'][] = $ci->geral->gWhere('categorias_id',$id);
		return $ci->geral->select('tb_subcategorias',$options);
	}
}

if(!function_exists('getPost')){
	function getPost(){
		$ci = & get_instance();
		$ci->load->model('Geral_model','geral');
		$options['orders'] = ['pub_id'=>'desc'];
		$options['joins'][] = $ci->geral->gJoin('tb_usuario','usuario_id','tb_publicacao','usuario_id');
		$options['joins'][] = $ci->geral->gJoin('dgt.tb_pessoa_fisica','pf_id','tb_usuario','usuario_pf');
		$options['joins'][] = $ci->geral->gJoin('dgtlgpd.tb_pessoa_fisica','pf_id','dgt.tb_pessoa_fisica','pf_id_lgpd');
		$options['wheres'][] = $ci->geral->gWhere("pub_status",true);
		$options['limit'] = 6;
		return $ci->geral->select('tb_publicacao',$options);
	}
}

if(!function_exists('getBanner')){
	function getBanner($not=array(),$type = 2){
		$ci = & get_instance();
		$ci->load->model('Geral_model','geral');
		$options['orders'] = ['banner_id'=>'desc'];
		$options['wheres'][] = $ci->geral->gWhere("banner_tipo",$type);
		$options['wheres'][] = $ci->geral->gWhere("banner_status",true);
		$options['wheres'][] = $ci->geral->gWhere("banner_id",$not,'not_in');
		return $ci->geral->select('tb_banner_site',$options,'row');
	}
}
