<?php

/**
 * Description of Upload_model
 *
 * @author vhbarboza
 */
class Upload_model extends CI_Model {

    public function uploadFile($file, $formatoArquivo = '*', $path = 'public/retorno', $encrypt = false) {
        $config['upload_path'] = $path;
        $config['allowed_types'] = $formatoArquivo;
        $config['max_size'] = 100000;
        $config['max_width'] = 20524;
        $config['max_height'] = 20500;
        if ($encrypt) {
            $config['encrypt_name'] = TRUE;
        }

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)):
            $error = array('error' => $this->upload->display_errors());
            debug($error);
        else:
            return array('upload_data' => $this->upload->data());
        endif;
    }

}
