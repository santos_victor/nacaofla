<?php

class Boleto_model extends CI_Model {

    protected $table = 'tb_boleto';
    protected $primary = 'bol_id';
    protected $usuario = 'usuarios_id';
    protected $fk_table = 'tb_usuario';
    protected $fk_pk = 'usuario_id';
    protected $remessa = 'gerou_remessa';
    protected $tabeleRemessa = 'tb_remessa';
    protected $tabeleRetorno = 'retorno';

    public function insertBoleto($dados) {
        $this->db->insert($this->table, $dados);
        return $this->db->insert_id();
    }

    public function findId($id) {
        $this->db->where($this->primary, $id);
        $this->db->join($this->fk_table, "$this->usuario  = $this->fk_pk");
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');
        return $this->db->get($this->table)->row_array();
    }

    public function findAllId($id) {
        $this->db->where_in($this->primary, $id);
        $this->db->join($this->fk_table, "$this->usuario  = $this->fk_pk");
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');
        return $this->db->get($this->table)->result_array();
    }
    
    public function selectBoletosBaixaRetorno(){
        $this->db->select('*');
        $this->db->join('tb_remessa', 'tb_remessa.remessa_id = tb_boleto.remessa_id');
        $this->db->where('gerou_remessa', 1);
        return $this->db->get($this->table)->result_array();
    }

    public function findToUser($usuario_id) {
        $this->db->where($this->usuario, $usuario_id);
        $this->db->join($this->fk_table, "$this->usuario  = $this->fk_pk");
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');
        return $this->db->get($this->table)->result_array();
    }

    public function selectBoletosRemessa($idRemessa) {        
        $this->db->where('tb_boleto.gerou_remessa', 1);
        $this->db->where('tb_boleto.remessa_id', $idRemessa);
        $this->db->join('tb_remessa', 'tb_remessa.remessa_id = tb_boleto.remessa_id');
        $this->db->join($this->fk_table, "$this->usuario  = $this->fk_pk");
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');        
        return $this->db->get($this->table)->result_array();
    }

    public function insereRemessa($dados) {
        $this->db->insert($this->tabeleRemessa, $dados);
        return $this->db->insert_id();
    }
    
    public function insereIdRemessa($dados2, $dtVencimento){
        $this->db->where('vecimento', $dtVencimento);
        $this->db->where('remessa_id', 0);
        $this->db->where('gerou_remessa', 0);
        return $this->db->update('tb_boleto', $dados2);
    }
    
    public function buscaRemessas(){
        $this->db->select('*');
        $this->db->from('tb_remessa');      
        return $this->db->get()->result_array();
    }
    
    public function buscaUsuarioBoleto(){  
        $this->db->group_by('usuarios_id');
        $this->db->join($this->fk_table, "$this->usuario  = $this->fk_pk");
        return $this->db->get($this->table)->result_array();
    }
    
    public function buscaUsuarioCriarBoleto($usu_id){  
        $this->db->group_by('usuarios_id');
        $this->db->where('usuarios_id', $usu_id);
        $this->db->join($this->fk_table, "$this->usuario  = $this->fk_pk");
        return $this->db->get($this->table)->result_array();
    }
    
    public function insereRetorno($dados){
        $this->db->insert($this->tabeleRetorno, $dados);
        return $this->db->insert_id();
    }

    public function baixaBoleto($nosso_numero, $data_ocorrencia){
        $id = $nosso_numero;
        $retorno = 1;
        $pago = 1;
        unset($nosso_numero);
        $this->db->set('retorno', $retorno);
        $this->db->set('pago', $pago);
        $this->db->set('data_pagamento', $data_ocorrencia);
        $this->db->where('bol_id', $id);
        $boletosComBaixa = $this->db->update('tb_boleto'); 
        return $boletosComBaixa;       
    }
    
    public function selectRemessaId($nosso_numero){
        $this->db->select('*');         
        $this->db->where('bol_id', $nosso_numero);      
        return $this->db->get($this->table)->row_array();
    }
    
    public function selectRetorno(){
        $this->db->select('*');  
        return $this->db->get($this->tabeleRetorno)->result_array();
    }
    
    public function verificaRetornoGerado($selectRemessaId){
        $this->db->select('*');
        $this->db->where('rem_id', $selectRemessaId);
        return $this->db->get($this->tabeleRetorno)->row_array();
    }
    
    public function selectBoletosBaixados(){
        $this->db->select('*');
        $this->db->join('tb_remessa', 'tb_remessa.remessa_id = tb_boleto.remessa_id');
        $this->db->join('tb_usuario', 'usuario_id = usuarios_id');
        $this->db->join('tb_pessoa_fisica', 'usuario_id_pf = pf_id');       
        $this->db->where('retorno', 1);
        $this->db->where('pago', 1);
        return $this->db->get($this->table)->result_array();
    }
    
}
