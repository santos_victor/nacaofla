<?php

/**
 * Description of Count_model
 *
 * @author vhbarboza
 */
class Count_model extends CI_Model {

    public function countFiliados() {
        $this->db->select('*');
        $this->db->from('tb_usuario');
        $this->db->where('usuario_perfil_id', 1);
        $result = $this->db->count_all_results();
        return $result;
    }

    public function countFiliadosHoje() {
        $data = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('tb_pessoa_fisica');
        $this->db->join('tb_usuario', 'pf_id = usuario_id_pf');
        $this->db->where('pf_dta_cadastro', $data);
        $this->db->where('usuario_perfil_id', 1);
        $result = $this->db->count_all_results();
        return $result;
    }
    
    public function countFiliadosMes(){
        $primeiradata = date('Y-m-01');
        $month = date('m');
        $year = date('Y');
        $ultimodia = (cal_days_in_month(CAL_GREGORIAN, $month, $year));
        //debug($ultimodia);
        $ultimadata = date("Y-m-$ultimodia");
        $this->db->from('tb_pessoa_fisica');
        $this->db->join('tb_usuario', 'pf_id = usuario_id_pf');
        $this->db->where('usuario_perfil_id', 1);
        $this->db->where('pf_dta_cadastro BETWEEN"'. $primeiradata . '"and"' . $ultimadata . '"');
        return $this->db->count_all_results();
    }

}
