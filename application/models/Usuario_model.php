<?php

class Usuario_model extends CI_Model{
    
    function selectUsuario($login, $senha, $tipo_usuario){
        $this->db->where('usuario_email', $login);
        $this->db->where('usuario_senha', $senha);
        $this->db->where('usuario_perfil_site_id', $tipo_usuario);
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');
        $usuario = $this->db->get('tb_usuario')->row_array();
        return $usuario;
    }
    
    function selectFiliados(){
        $this->db->select('*');
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');
        $this->db->join('estados', 'pf_uf = sigla');
        $this->db->where('usuario_perfil_id', 1);
        return $this->db->get('tb_usuario')->result_array();
    }
    
    function excluirPessaFisica($pf_id){
        $this->db->where("pf_id", $pf_id);
        $this->db->delete("tb_pessoa_fisica");
    }
    
    function excluirUsuario($usuario_id){
        $this->db->where("usuario_id", $usuario_id);
        $this->db->delete("tb_usuario");
    }
    
    function selectFiliado($id){
        $this->db->select('*');
        $this->db->join('tb_usuario', 'pf_id = usuario_id_pf');
        $this->db->where('pf_id', $id);
        return $this->db->get('tb_pessoa_fisica')->row_array();
    }
    
    function updatePf($dados) {
        $id = $dados['pf_id'];
        unset($dados['pf_id']);
        $this->db->where('pf_id', $id);
        return $this->db->update('tb_pessoa_fisica', $dados);
    }

    function updateUsuario($dados_usuario) {
        $id = $dados_usuario['usuario_id'];
        unset($dados_usuario['usuario_id']);
        $this->db->where('usuario_id', $id);
        return $this->db->update('tb_usuario', $dados_usuario);
    }
    
    public function inserePf($dados) {
        $this->db->insert('tb_pessoa_fisica', $dados);
        return $this->db->insert_id();
    }
    
    public function insereUsuario($dados) {
        $this->db->insert('tb_usuario', $dados);
        return $this->db->insert_id();
    }
    
    function selectUsuariosAdmin(){
        $this->db->select('*');
        $this->db->join('tb_pessoa_fisica', 'pf_id = usuario_id_pf');
        $this->db->join('estados', 'pf_uf = sigla');
        $this->db->where('usuario_perfil_id = 0 or usuario_perfil_site_id = 1');
        return $this->db->get('tb_usuario')->result_array();
    }  
    
    function selectCpf($cpf){
        $this->db->select('*');
        $this->db->from('tb_pessoa_fisica');
        $this->db->where('pf_cpf', $cpf);
        return $this->db->get()->row_array;
    }
    
    
}

    