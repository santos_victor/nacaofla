<?php

/**
 * Description of Estados_model
 *
 * @author vhbarboza
 */
class Estados_model  extends CI_Model{
    
    function selectEstados(){
        $this->db->select('*');        
        return $this->db->get('estados')->result_array();
    }
}
