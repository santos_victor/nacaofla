    "description": "The CodeIgniter framework",
    "name": "codeigniter/framework",
    "type": "project",
    "homepage": "https://codeigniter.com",
    "license": "MIT",
    "support": {
        "forum": "http://forum.codeigniter.com/",
        "wiki": "https://github.com/bcit-ci/CodeIgniter/wiki",
        "slack": "https://codeigniterchat.slack.com",
        "source": "https://github.com/bcit-ci/CodeIgniter"
    },
    "require": {
        "php": ">=5.3.0",
        "spipu/html2pdf": "^5.2",
        "mpdf/mpdf": "^8.0"
    },
    "suggest": {
        "paragonie/random_compat": "Provides better randomness in PHP 5.x"
    },
    "require-dev": {

        "psr-4": {
            "Mpdf\\": "tests/Mpdf"
        },

        "files": [
            "src/functions-dev.php"
        ],

        "mikey179/vfsstream": "1.1.*"
    },
    "autoload": {
        "psr-4": {
            "Mpdf\\": "src/"
        },

        "files": ["app/helpers.php"]
    }