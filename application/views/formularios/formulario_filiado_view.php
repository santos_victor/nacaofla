<div class="container">
    <div class="card card" style="margin-top: 20px">
        <div class="card-header text-center" style="background-color: #e30613">
            <h1 class="text text-white"><b>Formulário de Filiação</b></h1>
        </div>
        <div class="card-body">
            <?= validation_errors("<p class='alert alert-danger'>", "</p>") ?>
            <?php if ($this->session->flashdata("success")): ?>
                <center><div class="alert alert-success"><?= $this->session->flashdata("success") ?></div></center>
            <?php endif; ?>
                <?php if ($this->session->flashdata("danger")): ?>
                <center><div class="alert alert-danger"><?= $this->session->flashdata("danger") ?></div></center>
            <?php endif; ?>
            <form name="formCadastro" class="form" method="post" action="<?= base_url() ?>formulario/filiacao/insert" enctype="multipart/form-data">            
                <input type="hidden" value="0" name="perfil_filiado">
                <div class="card-body card-block"> 
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Nome Completo</label>
                            <input type="text" id="nome" class="form-control" name="nome" value="<?= set_value('nome', "") ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="cpf" class="form-control-label">CPF</label>
                            <input type="text" id="cpf" class="form-control" name="cpf" value="<?= set_value('cpf', "") ?>" data-inputmask='"mask": "999.999.999-99"' data-mask>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="rg" class="form-control-label">RG</label>
                            <input type="text" id="rg" class="form-control" name="rg" value="<?= set_value('rg', "") ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="orgao" class="form-control-label">Órgão Emissor</label>
                            <input type="text" id="rg" class="form-control" name="orgao" value="<?= set_value('orgao', "") ?>">
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="nascimento" class="form-control-label">Data de Nascimento</label>
                            <input type="date" id="nascimento" class="form-control" name="nascimento" value="<?= set_value('nascimento', "") ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Telefone Fixo</label>
                            <input type="text" id="telefone" class="form-control" name="telefone" value="<?= set_value('telefone', "") ?>" data-inputmask='"mask": "(99) 9999-9999"' data-mask>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Celular</label>
                            <input type="text" id="celular" class="form-control" name="celular" value="<?= set_value('celular', "") ?>" data-inputmask='"mask": "(99) 99999-9999"' data-mask>
                        </div>

                        <div class="form-group col-md-3">
                            <label for="cep" class="form-control-label">CEP</label>
                            <input type="text" id="cep" class="form-control" name="cep" onblur="pesquisacep(this.value);" value="<?= set_value('cep', "") ?>" data-inputmask='"mask": "99999-999"' data-mask>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Rua</label>
                            <input type="text" id="rua" class="form-control" name="endereco" value="<?= set_value('endereco', "") ?>">
                        </div>         
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Cidade</label>
                            <input type="text" id="cidade" class="form-control" name="cidade" value="<?= set_value('cidade', "") ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="uf" class="form-control-label">UF</label>
                            <input type="text" id="uf" class="form-control" name="uf" value="<?= set_value('uf', "") ?>">                              
                        </div>

                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Bairro</label>
                            <input type="text" id="bairro" class="form-control" name="bairro" value="<?= set_value('bairro', "") ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="sexo" class="form-control-label">Sexo</label>
                            <select class="form-control" name="sexo" id="sexo">
                                <option value="" selected="" readonly>Selecionar...</option>                                                
                                <option value="M">Masculino</option>
                                <option value="F">Feminino</option>
                                <option value="PNR">Prefiro não responder</option>
                                <option value="O">Outros</option>
                            </select>    
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">E-mail</label>
                            <input type="email" id="nome" class="form-control" name="email" value="<?= set_value('email', "") ?>">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Senha</label>
                            <input type="password" id="nome" class="form-control" name="senha" value="">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="nome" class="form-control-label">Confirmar Senha</label>
                            <input type="password" id="nome" class="form-control" name="confirmarsenha" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="icheck-success d-inline"> 
                                <div class="alert alert-light">
                                    <p style="text-align: justify">Declaro-me ciente de que estou me filiando à Associação dos Torcedores da Nação do Clubes de Regatas do Flamengo, 
                                        e que receberei no email constante em meu cadastro as mensalidades em arquivo formato PDF, estando ciente de todos
                                        os direitos e obrigações gerados, expostos em seu estatuto e na legislação civil em geral.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <div class="icheck-success d-inline">
                                <input type="checkbox" id="checkboxSuccess3" name="aceita_termos">
                                <label for="checkboxSuccess3">
                                    Li e aceito os termos
                                </label>
                            </div><br>

                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="submit" class="btn btn-danger btn-lg" value="ENVIAR">
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
</div>
<!-- /.card -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Caro Flamenguista,</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <p>Obrigado por associar-se ao movimento independente da torcida rubro-negra que revolucionará a história do nosso amado Mengão e de seus torcedores!</p>
                    <p>As mensalidades serão pagas por meio de boleto bancário, que será encaminhado para o e-mail do cadastro.</p>
                    <p>Caso o email não esteja em sua caixa de entrada, verifique na caixa Antispam.</p>
                    <p>Ajude a divulgar nossa associação, no site <a href="https://www.torcedoresdanacaofla.com.br" target="_blank">www.torcedoresdanacaofla.com.br</a> no selo “Indique um amigo” , você pode indicar quantos flamenguistas você desejar. </p>
                    <p>Juntos ajudaremos o Flamengo a ser maior e melhor do que já é. Contamos com todos vocês.</p>
                    <p>Saudações rubro-negras.</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>

            </div>
        </div>
    </div>
</div>
<?php
if ($this->session->flashdata("success")) {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#exampleModal').modal('show');
        });
    </script>
    <?php
}
?>
