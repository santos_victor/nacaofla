<div class="container">
    <div class="card" style="margin-top: 20px">
        <div class="card-header text-center" style="background-color: #e30613">
            <h1 class="text text-white"><b>Formulário de Indicação</b></h1>
        </div>
        <div class="card-body">
            <?= validation_errors("<p class='alert alert-danger'>", "</p>") ?>
            <?php if ($this->session->flashdata("success")): ?>
                <center><div class="alert alert-success"><?= $this->session->flashdata("success") ?></div></center>
            <?php endif; ?>
            <form class="form" method="post" action="<?= base_url() ?>enviar/convite" enctype="multipart/form-data">            
                <input type="hidden" value="0" name="perfil_filiado">
                <div class="card-body card-block"> 
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="email" class="form-control-label">Email</label>
                            <input type="email" id="email" class="form-control" name="email" value="<?= set_value('email', "") ?>" placeholder="Digite o email do seu amigo">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="nome" class="form-control-label">Nome ou apelido</label>
                            <input type="text" id="nome" class="form-control" name="nome" value="<?= set_value('cpf', "") ?>" placeholder="Digite o nome ou apelido pelo qual seu amigo lhe conhece">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                             <label for="nome" class="form-control-label">Mensagem</label>
                            <div class="mb-3">
                                <textarea class="form-control" rows="5" name="mensagem"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <input type="submit" class="btn btn-danger btn-lg swalDefaultSuccess" value="ENVIAR">
                </div>
            </form>
        </div>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->

