<div class="container">
    <div class="card">
        <div class="card-header"> <h1 class="text-center"><img src="<?= base_url() ?>public/images/logo-nacao-fla-01.png" style="opacity: .8; width: 200px"></h1></div>
        <div class="card-body">
            <h5 class="card-title">Olá <?= $nome ?></h5>
            <p class="card-text"><?= $mensagem ?></p>
            <a href="https://www.torcedoresdanacaofla.org.br/quero-ser-filiado/" class="btn btn-danger" target="_blank">Filie-se Agora!</a>
        </div>
        <footer class="bg-light text-center text-lg-start">
            <!-- Copyright -->
            <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2);">
                © <?= date('Y') ?> Copyright:
                <a class="text-dark" href="https://www.torcedoresdanacaofla.org.br/quero-ser-filiado/">Nação Fla</a>
            </div>
            <!-- Copyright -->
        </footer>
    </div>   
</div>