<!-- /.login-logo -->
<div class="card card-outline card-danger">
    <div class="card-header text-center">
        <img src="<?= base_url() ?>public/images/logo-nacao-fla-01.png" style="width: 200px">
    </div>
    <div class="card-body"> 
        <form action="<?= base_url() ?>autenticar" method="post">
            <div class="input-group mb-3">
                <input type="email" class="form-control" placeholder="Email" name="usuario">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" class="form-control" placeholder="Senha" name="senha">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2"></div>
                <div class="col-8">
                    <div class="icheck-primary">
                        <button type="submit" class="btn btn-danger btn-block">Login</button>
                    </div> 
                </div>
                <div class="col-2"></div>              
            </div>
        </form>       
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->

