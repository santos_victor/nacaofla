<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Visualizar Retorno</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Retorno</a></li>
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">            
            <?php if ($this->session->flashdata("danger")): ?>
                <center><div class="alert alert-danger"><?= $this->session->flashdata("danger") ?></div></center>
            <?php endif; ?>
            <!-- Info boxes -->
            <form class="form" method="post" action="<?= base_url() ?>visualizar_arquivo_retorno" enctype="multipart/form-data">
                <div class="row">
                    <div class="form-group col-6 col-sm-12 col-md-6">
                        <label for="sexo" class="form-control-label">Mês</label>
                        <input type="file" name="retorno" class="form-control">
                    </div>
                    <div class="form-group col-6 col-sm-12 col-md-6">
                        <label for="data" class="form-control-label">Data</label><br>
                        <input type="text" value="<?= date('d/m/Y') ?>" readonly name="data" class="form-control">                       
                    </div>                    
                </div>
                <div class="row">
                    <div class="form-group col-4 col-sm-12 col-md-4">
                        <input type="submit" class="btn btn-primary" value="Visualizar">
                    </div>
                </div>
            </form>            
            <!-- /.row -->            
        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
   
</div>
<!-- /.content-wrapper -->

