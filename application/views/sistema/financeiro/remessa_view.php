<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Gerar Remessa</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Remessa</a></li>
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">            
            <?php if ($this->session->flashdata("danger")): ?>
                <center><div class="alert alert-danger"><?= $this->session->flashdata("danger") ?></div></center>
            <?php endif; ?>
            <!-- Info boxes -->
            <form class="form" method="post" action="<?= base_url() ?>gerar_remessa">
                <div class="row">
                    <div class="form-group col-6 col-sm-12 col-md-6">
                        <label for="sexo" class="form-control-label">Mês</label>
                        <select class="form-control" name="mes" id="mes">
                            <option value="01" <?= date('m') == '01' ? 'selected' : '' ?>>Janairo</option>
                            <option value="02" <?= date('m') == '02' ? 'selected' : '' ?>>Fevereiro</option>
                            <option value="03" <?= date('m') == '03' ? 'selected' : '' ?>>Março</option>
                            <option value="04" <?= date('m') == '04' ? 'selected' : '' ?>>Abril</option>
                            <option value="05" <?= date('m') == '05' ? 'selected' : '' ?>>Maio</option>
                            <option value="06" <?= date('m') == '06' ? 'selected' : '' ?>>Junho</option>
                            <option value="07" <?= date('m') == '07' ? 'selected' : '' ?>>Julho</option>
                            <option value="08" <?= date('m') == '08' ? 'selected' : '' ?>>Agosto</option>
                            <option value="09" <?= date('m') == '09' ? 'selected' : '' ?>>Setembro</option>
                            <option value="10" <?= date('m') == '10' ? 'selected' : '' ?>>Outubro</option>
                            <option value="11" <?= date('m') == '11' ? 'selected' : '' ?>>Novembro</option>
                            <option value="12" <?= date('m') == '12' ? 'selected' : '' ?>>Dezembro</option>
                        </select>
                    </div>
                    <div class="form-group col-6 col-sm-12 col-md-6">
                        <label for="sexo" class="form-control-label">Ano</label><br>
                        <input type="text" value="<?= date('Y') ?>" readonly name="ano" class="form-control">                       
                    </div>                    
                </div>
                <div class="row">
                    <div class="form-group col-4 col-sm-12 col-md-4">
                        <input type="submit" class="btn btn-primary" value="Gerar">
                    </div>
                </div>
            </form>

            <!-- /.row -->            
        </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lista de Remessas Geradas</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID Remessa</th>
                                        <th>Mês / Ano</th>
                                        <th>Nome do arquivo</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($remessas as $remessa) : ?>
                                        <tr>
                                            <td><?= $remessa['remessa_id'] ?></td>
                                            <td><?= $remessa['dta_importacao'] ?></td>
                                            <td><?= $remessa['nome_arquivo'] ?></td>
                                        </tr>  
                                    <?php endforeach; ?>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
<!-- /.content-wrapper -->


