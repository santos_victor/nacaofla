<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Boletos Pagos</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Boletos</a></li>
                        <li class="breadcrumb-item active">Home</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lista de Boletos Pagos</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>CPF</th>
                                        <th>Data de Vencimento</th>                                        
                                        <th>Valor</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($boletosBaixados as $boleto) : ?>
                                        <tr>
                                            <td><?= $boleto['pf_nome'] ?></td>
                                            <td><?= $boleto['pf_cpf'] ?></td>
                                            <td><?= formataDateAll($boleto['vencimento'],'-') ?></td>
                                            <td><?= 'R$ '. number_format($boleto['valor'], 2 , ',', ' ') ?></td>
                                        </tr>  
                                    <?php endforeach; ?>
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
</div>
<!-- /.content-wrapper -->


