<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Usuário</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Usuário</a></li>
                        <li class="breadcrumb-item active">Cadastro</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                 

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Dados de Usuário</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <?= validation_errors("<p class='alert alert-danger'>", "</p>") ?>
                            <?php if ($this->session->flashdata("success")): ?>
                                <center><div class="alert alert-success"><?= $this->session->flashdata("success") ?></div></center>
                            <?php endif; ?>
                            <form class="form" method="post" action="<?= base_url() ?>cadastrar/usuario" enctype="multipart/form-data">                               
                                <div class="card-body card-block"> 
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Nome</label>
                                            <input type="text" id="nome" class="form-control" name="nome" 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="cpf" class="form-control-label">CPF</label>
                                            <input type="text" id="cpf" class="form-control" name="cpf" data-inputmask='"mask": "999.999.999-99"' data-mask
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="rg" class="form-control-label">RG</label>
                                            <input type="text" id="rg" class="form-control" name="rg" 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="orgao" class="form-control-label">Órgão Emissor</label>
                                            <input type="text" id="rg" class="form-control" name="orgao" value="<?= set_value('orgao', "") ?>">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="nascimento" class="form-control-label">Data de Nascimento</label>
                                            <input type="date" id="nascimento" class="form-control" name="nascimento" 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Telefone Fixo</label>
                                            <input type="text" id="telefone" class="form-control" name="telefone" data-inputmask='"mask": "(99) 9999-9999"' data-mask 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Celular</label>
                                            <input type="text" id="celular" class="form-control" name="celular" data-inputmask='"mask": "(99) 99999-9999"' data-mask
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="cep" class="form-control-label">CEP</label>
                                            <input type="text" id="cep" class="form-control" name="cep" onblur="pesquisacep(this.value);"
                                                   data-inputmask='"mask": "99999-999"' data-mask value="">
                                        </div>                                       

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Bairro</label>
                                            <input type="text" id="bairro" class="form-control" name="bairro" 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Cidade</label>
                                            <input type="text" id="cidade" class="form-control" name="cidade" 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">                                         
                                            <label for="uf" class="form-control-label">UF</label>
                                            <input type="text" id="uf" class="form-control" name="uf" value="<?= set_value('uf', "") ?>"> 
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Endereço</label>
                                            <input type="text" id="rua" class="form-control" name="endereco" 
                                                   value="">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="sexo" class="form-control-label">Sexo</label>
                                            <select class="form-control" name="sexo" id="sexo">
                                                <option value="" selected="" readonly>Selecionar...</option>                                                
                                                <option value="M">Masculino</option>
                                                <option value="F">Feminino</option>
                                            </select>    
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">E-mail</label>
                                            <input type="email" id="nome" class="form-control" name="email" 
                                                   value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="senha" class="form-control-label">Senha</label>
                                            <input type="password" id="senha" class="form-control" name="senha" value="">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="confirmarsenha" class="form-control-label">Confirmar Senha</label>
                                            <input type="password" id="confirmarsenha" class="form-control" name="confirmarsenha" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <input type="submit" class="btn btn-primary" value="Salvar">
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
