<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1><?= $titulo ?></h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#"><?= $titulo ?></a></li>
                        <li class="breadcrumb-item active">Alterar</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                 

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title"><?= $subtitulo ?></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form class="form" method="post" action="<?= base_url() ?>updateFiliado" enctype="multipart/form-data">
                                <input type="hidden" value="<?= $filiado['pf_id'] ?>" name="pf_id">
                                <input type="hidden" value="<?= $filiado['usuario_id'] ?>" name="usuario_id">
                                <input type="hidden" value="<?= $filiado['usuario_perfil_id'] ?>" name="perfil_id">
                                <div class="card-body card-block"> 
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Nome</label>
                                            <input type="text" id="nome" class="form-control" name="nome" 
                                                   value="<?= isset($filiado['pf_nome']) ? $filiado['pf_nome'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="cpf" class="form-control-label">CPF</label>
                                            <input type="text" id="cpf" class="form-control" name="cpf" 
                                                   value="<?= isset($filiado['pf_cpf']) ? $filiado['pf_cpf'] : '' ?>" data-inputmask='"mask": "999.999.999-99"' data-mask>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="rg" class="form-control-label">RG</label>
                                            <input type="text" id="rg" class="form-control" name="rg" 
                                                   value="<?= isset($filiado['pf_rg']) ? $filiado['pf_rg'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="orgao" class="form-control-label">Órgão Emissor</label>
                                            <input type="text" id="rg" class="form-control" name="orgao" 
                                                   value="<?= isset($filiado['pf_orgao_espedidor']) ? $filiado['pf_orgao_espedidor'] : '' ?>">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="nascimento" class="form-control-label">Data de Nascimento</label>
                                            <input type="date" id="nascimento" class="form-control" name="nascimento" 
                                                   value="<?= isset($filiado['pf_nascimento']) ? $filiado['pf_nascimento'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Telefone Fixo</label>
                                            <input type="text" id="telefone" class="form-control" name="telefone" 
                                                   value="<?= isset($filiado['pf_telefone']) ? $filiado['pf_telefone'] : '' ?>"  data-inputmask='"mask": "(99) 9999-9999"' data-mask>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Celular</label>
                                            <input type="text" id="celular" class="form-control" name="celular" 
                                                   value="<?= isset($filiado['pf_celular']) ? $filiado['pf_celular'] : '' ?>" data-inputmask='"mask": "(99) 99999-9999"' data-mask>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Endereço</label>
                                            <input type="text" id="endereco" class="form-control" name="endereco" 
                                                   value="<?= isset($filiado['pf_endereco']) ? $filiado['pf_endereco'] : '' ?>">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Bairro</label>
                                            <input type="text" id="bairro" class="form-control" name="bairro" 
                                                   value="<?= isset($filiado['pf_bairro']) ? $filiado['pf_bairro'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Cidade</label>
                                            <input type="text" id="cidade" class="form-control" name="cidade" 
                                                   value="<?= isset($filiado['pf_cidade']) ? $filiado['pf_cidade'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="uf" class="form-control-label">UF</label>
                                            <input type="text" id="cidade" class="form-control" name="uf" 
                                                   value="<?= isset($filiado['pf_uf']) ? $filiado['pf_uf'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="cep" class="form-control-label">CEP</label>
                                            <input type="text" class="form-control" name="cep" 
                                                   value="<?= isset($filiado['pf_cep']) ? $filiado['pf_cep'] : '' ?>">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="sexo" class="form-control-label">Sexo</label>
                                            <select class="form-control" name="sexo" id="sexo">
                                                <option value="" selected="" readonly>Selecionar...</option>                                                
                                                <option <?= (isset($filiado['pf_sexo']) && $filiado['pf_sexo'] == 'M') ? 'selected' : '' ?>
                                                    value="M">Masculino</option>
                                                <option <?= (isset($filiado['pf_sexo']) && $filiado['pf_sexo'] == 'F') ? 'selected' : '' ?>
                                                    value="F">Feminino</option>

                                            </select>    
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">E-mail</label>
                                            <input type="email" id="nome" class="form-control" name="email" 
                                                   value="<?= isset($filiado['usuario_email']) ? $filiado['usuario_email'] : '' ?>">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="nome" class="form-control-label">Senha</label>
                                            <input type="password" id="nome" class="form-control" name="senha" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <input type="submit" class="btn btn-primary" value="Salvar">
                                </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
