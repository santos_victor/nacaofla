<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Filiados</h1>
                </div>

                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Filiados</a></li>
                        <li class="breadcrumb-item active">Lista</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <?= validation_errors("<p class='alert alert-danger'>", "</p>") ?>
    <?php if ($this->session->flashdata("success")): ?>
        <center><div class="alert alert-success"><?= $this->session->flashdata("success") ?></div></center>
    <?php endif; ?>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">                 

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lista de Filiados</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Email</th>
                                        <th>Celular</th>
                                        <th>CPF</th>
                                        <th>Situação</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>
                                <?php
                                foreach ($boletosIdsUduarios as $key => $boletoIdUsuario) :
                                    $idBoletoUsuario[] = $boletoIdUsuario['usuarios_id'];
                                endforeach;
                                //debug($boletosIdsUduarios);
                                ?> 
                                <tbody>
                                    <?php foreach ($filiados as $filiado) : ?>                                    
                                        <tr>
                                            <td><?= $filiado['pf_nome'] ?></td>
                                            <td><?= $filiado['usuario_email'] ?></td>
                                            <td><?= $filiado['pf_celular'] ?></td>
                                            <td><?= $filiado['pf_cpf'] ?></td>
                                            <td><?= $filiado['pf_status'] = '1' ? 'Ativo' : 'Inativo' ?></td>
                                            <td>
                                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-info<?= $filiado['pf_id'] ?>">
                                                    <i class="nav-icon fas fa-info"></i>
                                                </button>
                                                <a href="<?= base_url() ?>editarFiliado/<?= $filiado['pf_id'] ?>" class="btn btn-warning" title="Editar"><i class="nav-icon fas fa-edit"></i></a>
                                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-danger<?= $filiado['pf_id'] ?>">
                                                    <i class="nav-icon fas fa-trash"></i>
                                                </button> 
                                                <a href="<?= base_url() ?>cadastro_boleto/<?= $filiado['usuario_id'] ?>/cad" 
                                                   <?= isset($idBoletoUsuario) && in_array($filiado['usuario_id'], $idBoletoUsuario) ? 'class="btn btn-primary disabled"' : 'class="btn btn-primary"' ?> >Gerar Boletos</a>
                                            </td>
                                        </tr>  
                                    <?php endforeach; ?>                                  
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal de exibição de dados -->
<?php foreach ($filiados as $filiado) : ?>
    <div class="modal fade" id="modal-info<?= $filiado['pf_id'] ?>">
        <div class="modal-dialog">
            <div class="modal-content bg-info">
                <div class="modal-header">
                    <h4 class="modal-title">Dados Gerais</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>Nome:</td>
                            <td></td>
                            <td><?= $filiado['pf_nome'] ?></td>
                        </tr>
                        <tr>
                            <td>CPF:</td>
                            <td></td>
                            <td><?= $filiado['pf_cpf'] ?></td>
                        </tr>
                        <tr>
                            <td>RG:</td>
                            <td></td>
                            <td><?= $filiado['pf_rg'] ?></td>
                        </tr>
                        <tr>
                            <td>Celular:</td>
                            <td></td>
                            <td><?= $filiado['pf_celular'] ?></td>
                        </tr>
                        <tr>
                            <td>Telefone Fixo:</td>
                            <td></td>
                            <td><?= $filiado['pf_telefone'] ?></td>
                        </tr>
                        <tr>
                            <td>Endereço:</td>
                            <td></td>
                            <td><?= $filiado['pf_endereco'] ?></td>
                        </tr>
                        <tr>
                            <td>Bairro:</td>
                            <td></td>
                            <td><?= $filiado['pf_bairro'] ?></td>
                        </tr>
                        <tr>
                            <td>Cidade:</td>
                            <td></td>
                            <td><?= $filiado['pf_cidade'] ?></td>
                        </tr>
                        <tr>
                            <td>UF:</td>
                            <td></td>
                            <td><?= $filiado['sigla'] ?></td>
                        </tr>
                        <tr>
                            <td>Sexo:</td>
                            <td></td>
                            <td><?= $filiado['pf_sexo'] ?></td>
                        </tr>
                        <tr>
                            <td>Nascimento:</td>
                            <td></td>
                            <td><?= $filiado['pf_nascimento'] ?></td>
                        </tr>
                        <tr>
                            <td>E-mail:</td>
                            <td></td>
                            <td><?= $filiado['usuario_email'] ?></td>
                        </tr>
                        <tr>
                            <td>Data do Cadastro:</td>
                            <td></td>
                            <td><?= $filiado['pf_dta_cadastro'] ?></td>
                        </tr>
                        <tr>
                            <td>Hora do Cadastro:</td>
                            <td></td>
                            <td><?= $filiado['pf_hora_cadastro'] ?></td>
                        </tr>
                        <tr>
                            <td>Cód. Indicação:</td>
                            <td></td>
                            <td><?= $filiado['pf_cod_usu_indicacao'] ?></td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Fechar</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>

<!-- Modal de exlusão -->
<?php foreach ($filiados as $filiado) : ?>
    <div class="modal fade" id="modal-danger<?= $filiado['pf_id'] ?>">
        <div class="modal-dialog">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Excluir Filiado</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Deseja excluir o filiado?</p>
                </div>
                <div class="modal-footer justify-content-between">
                    <a href="<?= base_url() ?>excluirFiliado/<?= $filiado['pf_id'] ?>/<?= $filiado['usuario_id'] ?>" class="btn btn-outline-light">Excluir</a>
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Fechar</button>

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>


