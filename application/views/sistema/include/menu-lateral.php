<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url() ?>sistema/home" class="brand-link">
        <img src="<?= base_url() ?>public/images/logo-nacao-fla-02.png" style="opacity: .8; width: 200px">
       
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <!--<div class="image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
            </div>-->
            <div class="info">
                <p style="margin-top: 20px; color: #FFF"><?= usuario('pf_nome') ?></p>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <a href="<?= base_url() ?>sistema/home" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url() ?>filiados" class="nav-link">
                        <i class="nav-icon fas fa-user"></i> <p> Filiados</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url() ?>usuarios/sistema" class="nav-link">
                        <i class="fas fa-users"></i> <p> Usuarios do Sistema</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?= base_url() ?>financeiro" class="nav-link">
                        <i class="fas fa-money-bill"></i> <p> Financeiro</p>
                    </a>
                </li>
                <!--<li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p> Administrador<i class="fas fa-angle-left right"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?= base_url() ?>usuarios/sistema" class="nav-link">
                                <i class="fas fa-users"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                    </ul>
                </li> -->
                <li class="nav-item">
                    <a href="<?= base_url() ?>logout" class="nav-link">
                        <i class="fas fa-sign-out"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
