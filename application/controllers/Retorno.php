<?php

class Retorno extends CI_Controller {

    protected $empresa = 'TORCEDORES DA NACAO DO CRF - CLUBES DE REGATAS FLAMENGO';
    protected $cnpj = '';
    protected $cep = '00000-000';
    protected $endereco = '';
    protected $bairro = '';
    protected $uf = '';
    protected $city = '';

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Usuario_model');
        $this->load->model('Estados_model');
        $this->load->model('Boleto_model');
        $this->load->model('Upload_model');
    }

    public function gerarRetorno() {

        $arquivo = $_FILES['retorno'];
        $data = $this->input->post('data');

        if (!empty($arquivo['name'])) {
            $this->Upload_model->uploadFile('retorno');
        }

        $retorno = \Eduardokum\LaravelBoleto\Cnab\Retorno\Factory::make('public/retorno/' . $arquivo["name"]);
        //echo $retorno->getBancoNome();

        $arquivoRetorno = $retorno->getDetalhes()->toArray();

        $boletos = $this->Boleto_model->selectBoletosBaixaRetorno();

        //debug($boletos);

        foreach ($boletos as $key => $boleto) {
            $boleto_id[] = $boleto['bol_id'];
        }

        foreach ($arquivoRetorno as $key => $value) {
            $nosso_numero = substr($value->numeroDocumento, 11, 10);
            $data_ocorrencia = $value->dataOcorrencia;
            
            if (in_array($nosso_numero, $boleto_id)) {
                $this->Boleto_model->baixaBoleto($nosso_numero, $data_ocorrencia);
                $selectRemessaId = $this->Boleto_model->selectRemessaId($nosso_numero);
            }
        }

        $dados = [
            'data_importacao' => $data,
            'nome_arquivo' => $arquivo['name'],
            'rem_id' => $selectRemessaId['remessa_id']
        ];

        $this->Boleto_model->insereRetorno($dados);
        $retorno->processar();
        //dd($retorno->getDetalhes()->toArray());        
        $this->retorno();
    }

    public function retorno() {
        $dados = [
            'tela' => 'sistema/financeiro/retorno_view'
        ];
        $dados['retornos'] = $this->Boleto_model->selectRetorno();
        $this->load->view('principalSistema', $dados);
    }

    public function visualizarRetorno() {
        $arquivo = $_FILES['retorno'];
        
        if (!empty($arquivo['name'])) {
            $this->Upload_model->uploadFile('retorno');
        }
        
        $retorno = \Eduardokum\LaravelBoleto\Cnab\Retorno\Factory::make('public/retorno/' . $arquivo["name"]);
        echo $retorno->getBancoNome();
        $retorno->processar();
        
//        $arquivoRetorno = $retorno->getDetalhes()->toArray();        
//        foreach ($arquivoRetorno as $key => $value) {
//            $nosso_numero = $value->dataOcorrencia;
//            debug($nosso_numero);            
//        }

        //debug($arquivoRetorno);
        dd($retorno->getDetalhes()->toArray());
    }

    public function telaVisualizarRetorno() {
        $dados = [
            'tela' => 'sistema/financeiro/visualizar_retorno_view'
        ];

        $this->load->view('principalSistema', $dados);
    }

}
