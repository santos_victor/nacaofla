<?php
/**
 * Description of Boleto
 *
 * @author vhbarboza
 */
class Boleto extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Usuario_model');
        $this->load->model('Estados_model');
        $this->load->model('Boleto_model');
    }
    
    public function boletosComBaixa(){
        $dados = [
            'tela' => 'sistema/financeiro/boletos_baixa_view'
        ];        
        $dados['boletosBaixados'] = $this->Boleto_model->selectBoletosBaixados();
        //debug($dados['boletosBaixados']);
        $this->load->view('principalSistema', $dados);
    }
}
