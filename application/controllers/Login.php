<?php

/**
 * Description of Login
 *
 * @author vhbarboza
 */
class Login extends CI_Controller {

 function __construct() {
        parent::__construct();
        $this->load->model('Usuario_model');
        date_default_timezone_set('America/Sao_Paulo');
    }
    
    public function index(){
        $dados = [
          'tela' => 'sistema/login/login_view' 
        ];
        $this->load->view('principalLogin', $dados);
    }
    
    public function autenticarLogin(){
       
        $login = $this->input->post('usuario');
        $senha = md5($this->input->post('senha'));
        $usuario = $this->Usuario_model->selectUsuario($login, $senha, $tipo_usuario = 1);        
        if ($usuario) {
            unset($usuario['usuario_senha']);
            $usuario['usuario_logado'] = 1;
            iniciar_sessao($usuario);
            $this->session->set_flashdata('success', 'Logado com sucesso');
            redirect('sistema/home');
        } else {
            $this->session->set_flashdata("danger", "Usuário ou senha inválida");
            redirect('login');
        }
    }
    
    public function logout() {
        finalizar_sessao();
        redirect('login');
    }
    
}
