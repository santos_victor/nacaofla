<?php

/**
 * Description of My_controller
 *
 * @author vhbarboza
 */
class My_controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Convite_model');
        $this->load->model('Usuario_model');
        $this->load->model('Estados_model');
        $this->load->library('session');
    }

    public function geraBoleto($nome = '', $endereco = '', $uf = '', $cidade = '', $cep = '', $email = '') {
        for ($i = 0; $i < 12; $i++) {
            //Criação do boleto
            $dados = array(
                'dias_de_prazo_para_pagamento' => 5,
                'taxa_boleto' => 0,
                'pedido' => array(
                    'nome' => 'Serviços de Desenvolvimento Web',
                    'quantidade' => '1',
                    'valor_unitario' => '5',
                    'numero' => 10000000025,
                    'aceite' => 'N',
                    'especie' => 'R$',
                    'especie_doc' => 'DM',
                ),
                'sacado' => array(
                    'nome' => $nome,
                    'endereco' => $endereco,
                    'cidade' => $cidade,
                    'uf' => $uf,
                    'cep' => $cep,
                ),
            );
            $dados['convenio'] = 568074;
            $dados['modalidade_cobranca'] = '01';

            //Converte Boleto para PDF
            $mpdf = new \Mpdf\Mpdf();
            $html = $this->load->view('sistema/boleto/boleto_view', ['dados' => $dados], true);
            $mpdf->shrink_tables_to_fit = 1;
            $stylesheet = file_get_contents('./public/tabela.css');
            $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS); // write the HTML into the PDF
            $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY); // write the HTML into the PDF
            //$mpdf->Output("public/anexos/{$nome}.pdf", \Mpdf\Output\Destination::FILE); // save to file because we can
            $mpdf->Output();
        }
        //Cria e envia o e-mail
        $this->load->library('email');
        $config = array(
            'protocol' => 'sendmail',
            'charset' => 'utf-8',
            'newline' => '\r\n',
            'mailtype' => 'html',
        );
        $this->email->initialize($config);
        //$this->email->from('contato@torcedoresdanacaofla.org.br', 'Nação Fla');
        $this->email->from('contato@torcedoresdanacaofla.org.br', 'Nação Fla');
        $this->email->to($email);
        $this->email->bcc('victor.hugo@codigosesolucoes.com.br');
        $nacao = 'Nação Fla';
        //$this->email->bcc('indiqueumamigonacaofla@gmail.com');
        $this->email->subject('Boletos de Filiação ' . $nacao);
        $logo = base_url() . 'public/images/logo-nacao-fla-01.png';
        $html_email = ("               
                <div class='container' style='text-align:center; padding:30px'>
                     <h1 class='text-center'><img src='{$logo}' style='opacity: .8; width: 500px; margin-bottom: -50px'></h1></div>
                    <div class='text-uppercase' style='text-align:center'><h2> Seja bem vindo(a) {$nome}!</h2></div>
                    <div class='container'>
                        <p style='text-align:center; padding: 20px 200px; text-align: justify;'>Prezado(a),</p>  
                        <p style='text-align:center; padding: 20px 200px; text-align: justify;'>Boletos seguem em anexo</p>                        
                    </div>
                    <address style='text-align:center; background-color: #e83742; padding: 20px'>
                    <strong style='color: #fff'>Acesse nosso site</strong>
                    <address>
                        <strong><a href='https://www.torcedoresdanacaofla.org.br/' style='text-align:center; color: #000'>www.torcedoresdanacaofla.org.br</a></strong><br>
                    </address>
                </div>");
        $this->email->message($html_email);
        $this->email->attach("public/anexos/{$nome}.pdf");
        $this->email->send();
    }

    function converteParapdf($dados) {
        $this->load->library('pdf');
        $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);

        // Gera o boleto
        $pdf->SetTitle('My Title');
        $pdf->SetHeaderMargin(30);
        $pdf->SetTopMargin(20);
        $pdf->setFooterMargin(20);
        $pdf->SetAutoPageBreak(true);
        $pdf->SetAuthor('Author');
        $pdf->SetDisplayMode('real', 'default');
        $pdf->AddPage();
        $html = $this->load->view('sistema/boleto/boleto_view', ['dados' => $dados], true);
        $pdf->WriteHTML($html);
        $pdf->Output('My-File-Name.pdf', 'F');
    }

    function converteParaMpdf($dados) {
        $mpdf = new \Mpdf\Mpdf();
        $html = $this->load->view('sistema/boleto/boleto_view', ['dados' => $dados], true);
        $mpdf->shrink_tables_to_fit = 1;
        $stylesheet = file_get_contents('./public/tabela.css');
        $mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS); // write the HTML into the PDF
        $mpdf->WriteHTML($html, \Mpdf\HTMLParserMode::HTML_BODY); // write the HTML into the PDF
        $mpdf->Output('public/anexos/Boleto.pdf', \Mpdf\Output\Destination::FILE); // save to file because we can
    }

}
