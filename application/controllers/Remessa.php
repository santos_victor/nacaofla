<?php

class Remessa extends CI_Controller {

    protected $empresa = 'TORCEDORES DA NACAO DO CRF - CLUBES DE REGATAS FLAMENGO';
    protected $cnpj = '';
    protected $cep = '00000-000';
    protected $endereco = '';
    protected $bairro = '';
    protected $uf = '';
    protected $city = '';

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Usuario_model');
        $this->load->model('Estados_model');
        $this->load->model('Boleto_model');
    }

    public function geraRemessa($idRemessa) {

        $boletos = $this->Boleto_model->selectBoletosRemessa($idRemessa);

        if (!empty($boletos)) {
            foreach ($boletos as $boleto) {
                $beneficiario = $this->getBeneficiario();

                $pagador = $this->getPagador(
                        $boleto['pf_cpf'],
                        $boleto['pf_nome'],
                        $boleto['pf_cep'],
                        $boleto['pf_endereco'],
                        $boleto['pf_bairro'],
                        $boleto['pf_uf'],
                        $boleto['pf_cidade']);
                $bancoob[] = $this->getBoleto($boleto['bol_id'], $boleto['vencimento'], $boleto['valor'], $pagador, $beneficiario);
            }
            $this->genereteRemessa($boleto['remessa_id'], $boleto['nome_arquivo'], $bancoob, $beneficiario);
        }else{
            $this->session->set_flashdata("danger", "Não há remessas para gerar no momento");
            redirect('remessa');
        }
    }

    public function getBoleto($numeroDoc, $vencimento, $valor, \Eduardokum\LaravelBoleto\Pessoa $pagador, \Eduardokum\LaravelBoleto\Pessoa $beneficiario) {
        $bancoob = new Eduardokum\LaravelBoleto\Boleto\Banco\Bancoob;
        $bancoob->setDataVencimento(new \Carbon\Carbon($vencimento))
                ->setValor($valor)
                ->setNumero($numeroDoc)
                ->setNumeroDocumento($numeroDoc)
                ->setPagador($pagador)
                ->setBeneficiario($beneficiario)
                ->setCarteira(1)
                ->setAgencia(4001)
                ->setConvenio(568074)
                ->setConta(120838);
        return $bancoob;
    }

    private function getBeneficiario() {
        $beneficiario = new \Eduardokum\LaravelBoleto\Pessoa;
        $beneficiario->setDocumento($this->cnpj)
                ->setNome($this->empresa)
                ->setCep($this->cep)
                ->setEndereco($this->empresa)
                ->setBairro($this->bairro)
                ->setUf($this->uf)
                ->setCidade($this->city);
        return $beneficiario;
    }

    private function getPagador($documento, $nome, $cep, $endereco, $bairro, $uf, $cidade) {
        $pagador = new \Eduardokum\LaravelBoleto\Pessoa;
        $pagador->setDocumento($documento)
                ->setNome($nome)
                ->setCep($cep)
                ->setEndereco($endereco)
                ->setBairro($bairro)
                ->setUf($uf)
                ->setCidade($cidade);
        return $pagador;
    }

    public function genereteRemessa($remessa_id, $arquivo, $boletos, $beneficiario) {
        $remessa = new \Eduardokum\LaravelBoleto\Cnab\Remessa\Cnab240\Banco\Bancoob(
                [
            'idRemessa' => $remessa_id,
            'beneficiario' => $beneficiario,
            'carteira' => 1,
            'agencia' => 4001,
            'convenio' => 568074,
            'conta' => 120838,
        ]);

        $remessa->addBoletos($boletos);
        echo $remessa->download($filename = $arquivo);
        //echo $remessa->gerar();
    }

    public function remessa() {
        $dados = [
            'tela' => 'sistema/financeiro/remessa_view'
        ];
        $dados['remessas'] = $this->Boleto_model->buscaRemessas();
        $this->load->view('principalSistema', $dados);
    }

    public function gerarRemessaOld() {
        $mes = $this->input->post('mes');
        $ano = $this->input->post('ano');
        $arquivo = 'remessa_' . $mes . '_' . $ano . '.REM';

        $dados = [
            'dta_importacao' => $mes . '/' . $ano,
            'nome_arquivo' => $arquivo
        ];

        $boletos = $this->Boleto_model->selectBoletosRemessa();

        foreach ($boletos as $key => $boleto) {
            $mesBanco = explode("/", $boleto['dta_importacao']);
        }

        if (isset($mesBanco[0]) && $mesBanco[0] == $mes) {
            $this->session->set_flashdata("danger", "Remessa já gerada!");
            redirect('remessa');
        } elseif ($mes > date('m')) {
            $this->session->set_flashdata("danger", "O mês selecionado é diferente do mês em questão!");
            redirect('remessa');
        } else {
            $idRemessa = $this->Boleto_model->insereRemessa($dados);
            $dados2 = [
                'gerou_remessa' => 1,
                'remessa_id' => $idRemessa
            ];
            $this->Boleto_model->insereIdRemessa($dados2);
            $this->geraRemessa();
        }
        //$this->session->set_flashdata("success", "Cadastrado com sucesso!");
        //redirect('remessa');
    }

    public function gerarRemessa() {
        $mes = $this->input->post('mes');
        $ano = $this->input->post('ano');
        $arquivo = 'remessa_' . $mes . '_' . $ano . '.REM';

        $dados = [
            'dta_importacao' => $mes . '/' . $ano,
            'nome_arquivo' => $arquivo
        ];

        $idRemessa = $this->Boleto_model->insereRemessa($dados);
        $dtVencimento = date("d/m/Y", strtotime("+1 month", mktime(0, 0, 0, date('m'), 8, date('Y'))));
        $dados2 = [
            'gerou_remessa' => 1,
            'remessa_id' => $idRemessa,            
        ];
        $this->Boleto_model->insereIdRemessa($dados2, $dtVencimento);
        $this->geraRemessa($idRemessa);
    }

}
