<?php

defined('BASEPATH') OR exit('No direct script access allowe');
/**
 * Description of Financeiro
 *
 * @author vhbarboza
 */
class Financeiro extends CI_Controller {
   
    function __construct() {
        parent::__construct();        
        date_default_timezone_set('America/Sao_Paulo');
        
    }
    
    public function index(){
        $dados = [
          'tela' => 'sistema/financeiro/financeiro_view'  
        ];
        $this->load->view('principalSistema', $dados);
    }
}
