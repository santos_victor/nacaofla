<?php

/**
 * Description of Sistema
 *
 * @author vhbarboza
 */
class Sistema extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        $this->load->model('Count_model');
        date_default_timezone_set('America/Sao_Paulo');
    }
    
    public function index(){
        $dados = [
          'tela' => 'sistema/home_view'  
        ];
        $dados['totalFiliados'] = $this->Count_model->countFiliados();
        $dados['totalFiliadosHoje'] = $this->Count_model->countFiliadosHoje();
        $dados['totalDiliadosMes'] = $this->Count_model->countFiliadosMes();
        $this->load->view('principalSistema', $dados);
    }
            
}
