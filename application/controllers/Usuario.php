<?php

defined('BASEPATH') OR exit('No direct script access allowe');

class Usuario extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Usuario_model');
        $this->load->model('Estados_model');
        $this->load->model('Boleto_model');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function telaFiliados() {
        $dados = [
            'tela' => 'sistema/usuario/filiados_view'
        ];
        $dados['filiados'] = $this->Usuario_model->selectFiliados();
        $boletosUsuarios =  $this->Boleto_model->buscaUsuarioBoleto(); 
        
        $dados['boletosIdsUduarios'] = $boletosUsuarios;
        
        $this->load->view('principalSistema', $dados);
    }

    public function excluirFiliado($pf_id, $usuario_id) {
        $this->Usuario_model->excluirPessaFisica($pf_id);
        $this->Usuario_model->excluirUsuario($usuario_id);
        redirect('filiados');
    }

    public function editarFiliado($id) {
        $tipo = $this->input->get('tipo');
        $dados = [
            'tela' => 'sistema/usuario/alterar_filiado_view'
        ];

        if ($tipo == 'Admin') {
            $dados['titulo'] = 'Usuario';
            $dados['subtitulo'] = 'Dados de Usuario';
        } else {
            $dados['titulo'] = 'Filiado';
            $dados['subtitulo'] = 'Dados de Filiado';
        }
        $dados['filiado'] = $this->Usuario_model->selectFiliado($id);
        $dados['estados'] = $this->Estados_model->selectEstados();

        //debug($dados['estados']);
        $this->load->view('principalSistema', $dados);
    }

    public function updateFiliado() {

        $perfil_id = $this->input->post('perfil_id');

        $dados = [
            'pf_id' => $_POST['pf_id'],
            'pf_nome' => $_POST['nome'],
            'pf_cpf' => $_POST['cpf'],
            'pf_rg' => $_POST['rg'],
            'pf_orgao_espedidor' => $_POST['orgao'],
            'pf_nascimento' => $_POST['nascimento'],
            'pf_telefone' => $_POST['telefone'],
            'pf_celular' => $_POST['celular'],
            'pf_endereco' => $_POST['endereco'],
            'pf_bairro' => $_POST['bairro'],
            'pf_cidade' => $_POST['cidade'],
            'pf_uf' => $_POST['uf'],
            'pf_cep' => $_POST['cep'],
            'pf_sexo' => $_POST['sexo'],
        ];

        $this->Usuario_model->updatePf($dados);

        $dados_usuario = [
            'usuario_id' => $_POST['usuario_id'],
            'usuario_email' => $_POST['email'],
        ];
        if (!empty($_POST['senha'])) {
            $dados_usuario['usuario_senha'] = md5($_POST['senha']);
        }

        $this->Usuario_model->updateUsuario($dados_usuario);

        if ($perfil_id == 1) {
            $this->session->set_flashdata("success", "Cadastrado com sucesso!");
            redirect('filiados');
        } else {
            $this->session->set_flashdata("success", "Cadastrado com sucesso!");
            redirect('usuarios/sistema');
        }
    }

    public function telaUsuarios() {
        $dados = [
            'tela' => 'sistema/usuario/usuariosAdmin_view'
        ];
        $dados['usuarios'] = $this->Usuario_model->selectUsuariosAdmin();
        $this->load->view('principalSistema', $dados);
    }

    public function cadastroUsuario() {
        $dados = [
            'tela' => 'sistema/usuario/cadastroUsuario_view'
        ];
        $dados['estados'] = $this->Estados_model->selectEstados();
        $this->load->view('principalSistema', $dados);
    }

    public function cadastrarUsuario() {

        $this->form_validation->set_rules('senha', 'Senha', 'required');
        $this->form_validation->set_rules('confirmarsenha', 'Confirmar Senha', 'required|matches[senha]');
        $this->form_validation->set_rules('cpf', 'CPF', 'required|valid_cpf');
        $succcess = $this->form_validation->run();

        //debug($succcess);

        if ($succcess) {
            $dados = [
                'pf_nome' => $_POST['nome'],
                'pf_cpf' => $_POST['cpf'],
                'pf_rg' => $_POST['rg'],
                'pf_orgao_espedidor' => $_POST['orgao'],
                'pf_nascimento' => $_POST['nascimento'],
                'pf_telefone' => $_POST['telefone'],
                'pf_celular' => $_POST['celular'],
                'pf_endereco' => $_POST['endereco'],
                'pf_bairro' => $_POST['bairro'],
                'pf_cidade' => $_POST['cidade'],
                'pf_uf' => $_POST['uf'],
                'pf_cep' => $_POST['cep'],
                'pf_sexo' => $_POST['sexo'],
                'pf_dta_cadastro' => date('Y-m-d'),
                'pf_hora_cadastro' => date('h:i:s'),
                'pf_status' => 1
            ];

            $pessoa_fisica = $this->Usuario_model->inserePf($dados);

            $dados_usuario = [
                'usuario_id' => $_POST['usuario_id'],
                'usuario_email' => $_POST['email'],
                'usuario_senha' => md5($_POST['senha']),
                'usuario_id_pf' => $pessoa_fisica,
                'usuario_perfil_id' => 0,
                'usuario_perfil_site_id' => 1
            ];

            $this->Usuario_model->insereUsuario($dados_usuario);
            $this->session->set_flashdata("success", "Cadastrado com sucesso!");
            redirect('usuarios/sistema');
        } else {
            $this->cadastroUsuario();
        }
    }

    public function excluirUsuario($pf_id, $usuario_id) {
        $this->Usuario_model->excluirPessaFisica($pf_id);
        $this->Usuario_model->excluirUsuario($usuario_id);
        redirect('usuarios/sistema');
    }
    
    public function telaInfoBoleto(){
        $dados = [
          'tela' =>  'sistema/readme.html' 
        ];
        $this->load->view('principalLogin', $dados);
    }

}
