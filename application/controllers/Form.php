<?php

/**
 * Description of Form
 *
 * @author vhbarboza
 */
class Form extends CI_Controller {

    protected $empresa = 'TORCEDORES DA NACAO DO CRF - CLUBES DE REGATAS FLAMENGO';
    protected $cnpj = '40.150.353/0001-92';
    protected $cep = '00000-000';
    protected $endereco = 'Chacara 125 Lote D2';
    protected $bairro = 'Vicente Pires';
    protected $uf = 'DF';
    protected $city = 'Brasília';
    protected $demonstrativo = array('...', 'Mensalidade referente a filiação à Torcedores da Nação Fla.');
    protected $instrucoes = array('Sr. Caixa, Não cobrar multa após o vencimento.', 'Receber até 30 dias do vencimento.',
        'Em caso de dúvidas entre em contato pelo email:',
        'faleconosco@torcedoresdanacaofla.com.br');

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
        $this->load->model('Convite_model');
        $this->load->model('Usuario_model');
        $this->load->model('Estados_model');
        $this->load->model('Boleto_model');
    }

    public function formularioFiliacao() {
        $dados = [
            'tela' => 'formularios/formulario_filiado_view'
        ];
        $dados['estados'] = $this->Estados_model->selectEstados();

        $this->load->view('principalFormulario', $dados);
    }

    public function insertFiliado() {

        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('senha', 'Senha', 'required');
        $this->form_validation->set_rules('confirmarsenha', 'Confirma Senha', 'required|matches[senha]');
        $this->form_validation->set_rules('cpf', 'CPF', 'required|valid_cpf|is_unique[tb_pessoa_fisica.pf_cpf]');
        $this->form_validation->set_rules('rg', 'RG', 'required');
        $this->form_validation->set_rules('orgao', 'Órgão Emissor', 'required');
        $this->form_validation->set_rules('nascimento', 'Data de Nascimento', 'required');
        $this->form_validation->set_rules('uf', 'UF', 'required');
        $this->form_validation->set_rules('celular', 'Celular', 'required');
        $this->form_validation->set_rules('cep', 'CEP', 'required');
        $this->form_validation->set_rules('sexo', 'Sexo', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[tb_usuario.usuario_email]');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('aceita_termos', 'Li e aceito os termos', 'required');
        $succcess = $this->form_validation->run();

        $data = $_POST['nascimento'];

        list($ano, $mes, $dia) = explode('-', $data);
        $idade = $this->getIdade($mes, $dia, $ano);

        if ($idade < 18) {
            $this->session->set_flashdata("danger", "Cadastro somente para maiores de 18 anos!");
            $this->formularioFiliacao();
        } else {

            if ($succcess) {
                if (isset($_POST['aceita_termos'])) {
                    $marcado = 1;
                } else {
                    $marcado = 0;
                }
                $dados = [
                    'pf_nome' => $_POST['nome'],
                    'pf_cpf' => $_POST['cpf'],
                    'pf_rg' => $_POST['rg'],
                    'pf_orgao_espedidor' => $_POST['orgao'],
                    'pf_nascimento' => $_POST['nascimento'],
                    'pf_aceito_cond' => $marcado,
                    'pf_telefone' => $_POST['telefone'],
                    'pf_celular' => $_POST['celular'],
                    'pf_endereco' => $_POST['endereco'],
                    'pf_bairro' => $_POST['bairro'],
                    'pf_cidade' => $_POST['cidade'],
                    'pf_uf' => $_POST['uf'],
                    'pf_cep' => $_POST['cep'],
                    'pf_sexo' => $_POST['sexo'],
                    'pf_dta_cadastro' => date('Y-m-d'),
                    'pf_hora_cadastro' => date('H:i:s'),
                    'pf_dta_filiacao' => date('Y-m-d'),
                    'pf_status' => 1
                ];
                $pessoa_fisica = $this->Usuario_model->inserePf($dados);

                $dados_usuario = [
                    'usuario_email' => $_POST['email'],
                    'usuario_id_pf' => $pessoa_fisica,
                    'usuario_senha' => md5($_POST['senha']),
                    'usuario_perfil_id' => 1
                ];
                $usu_id = $this->Usuario_model->insereUsuario($dados_usuario);

                $this->cadastrandoBoletoFormulario($usu_id);

                $this->session->set_flashdata("success", "Filiado cadastrado com sucesso!");
                redirect('formulario/filiacao');
            } else {
                $this->formularioFiliacao();
            }
        }
    }

    public function formularioConvite() {
        $dados = [
            'tela' => 'formularios/formulario_convite_view'
        ];

        $this->load->view('principalFormulario', $dados);
    }

    public function enviarConvite() {
        $email = $this->input->post('email');
        $nome = $this->input->post('nome');
        $mensagem = $this->input->post('mensagem');

        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('mensagem', 'Mensagem', 'required');
        $succcess = $this->form_validation->run();

        if ($succcess) {
            $dados = [
                'convite_email' => $email,
                'convite_nm_apl' => $nome,
                'convite_mensagem' => $_POST['mensagem'],
                'convite_data' => date('Y-m-d'),
                'convite_hora' => date('H:i:s')
            ];

            $this->Convite_model->salvaConvite($dados);

            $this->load->library('email');

            $config = array(
                'protocol' => 'sendmail',
                'charset' => 'utf-8',
                'newline' => '\r\n',
                'mailtype' => 'html',
            );

            $this->email->initialize($config);
            $this->email->from('contato@torcedoresdanacaofla.org.br', 'Nação Fla');
            $this->email->to($email);
            $this->email->bcc('indiqueumamigonacaofla@gmail.com');
            $this->email->subject('Convite de ' . $nome);
            $logo = base_url() . 'public/images/logo-nacao-fla-01.png';
            $html = ("               
                <div class='container' style='text-align:center; padding:30px'>
                     <h1 class='text-center'><img src='{$logo}' style='opacity: .8; width: 500px; margin-bottom: -50px'></h1></div>
                    <div class='text-uppercase' style='text-align:center'><h2> Um convite de {$nome} para você!</h2></div>
                    <div class='container'>
                        <p style='text-align:center; padding: 20px 200px;  text-align: justify;'> {$mensagem} </p>                    
                        <p style='text-align:center'><a href='https://www.torcedoresdanacaofla.org.br/quero-ser-filiado/' 
                        style=' background: -webkit-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                         background: -moz-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                         background: -o-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                         background: -ms-linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                         background: linear-gradient(bottom, #E0E0E0, #F9F9F9 70%);
                         border: 1px solid #CCCCCE;
                         border-radius: 3px;
                         box-shadow: 0 3px 0 rgba(0, 0, 0, .3),
                                       0 2px 7px rgba(0, 0, 0, 0.2);
                         color: #e83742;
                         
                         font-family: 'Trebuchet MS';
                         font-size: 18px;
                         font-weight: bold;
                         line-height: 25px;
                         text-align: center;
                         text-decoration: none;
                         text-transform: uppercase;
                         text-shadow:1px 1px 0 #FFF;
                         padding: 10px 25px;
                         position: relative;
                         width: 80px;' 
                        target='_blank'>Filie-se Agora!</a></p>
                    </div>
                    <address style='text-align:center; background-color: #e83742; padding: 20px'>
                    <strong style='color: #fff'>Acesse nosso site</strong>
                    <address>
                        <strong><a href='https://www.torcedoresdanacaofla.org.br/' style='text-align:center; color: #000'>www.torcedoresdanacaofla.org.br</a></strong><br>
                    </address>
                </div>");
            $this->email->message($html);
            $this->email->attach('public/anexos/Projeto.pdf');
            $this->email->send();
            $this->session->set_flashdata("success", "Email enviado com sucesso!");
            redirect('formulario/convite');
        } else {
            $this->formularioConvite();
        }
    }

    function cadastrandoBoletoFormulario($usu_id) {
        // Gerar os boletos (RN: gerar 20 parcelas, valor 5 reais)
        $nParcelas = 21;
        $valor = 5;
        $dtVencimento = date("d/m/Y", strtotime("+1 month", mktime(0, 0, 0, date('m'), 8, date('Y'))));
        //$dtVencimentoMesAtual = date('d/m/Y');

        for ($p = 0; $p < $nParcelas; $p++) {
            $vencimento = $this->getVencimento($p, $dtVencimento);
            // Montar array que será gravado no banco
            $dadosBoleto = [
                'valor' => $valor++,
                'vencimento' => $vencimento,
                'usuarios_id' => $usu_id,
                'vencimento' => date('Y-m-d H:i:s')
            ];

            $boletos_id[] = $this->Boleto_model->insertBoleto($dadosBoleto);
        }

        // Gerar boletos
        $this->genereteBoleto($boletos_id);
    }

    function cadastrandoBoleto($usu_id, $cad = '') {

        $boletos = $this->Boleto_model->buscaUsuarioCriarBoleto($usu_id);

        if (empty($boletos) && $cad == 'cad') {
            // Gerar os boletos (RN: gerar 20 parcelas, valor 5 reais)
            $nParcelas = 20;
            $valor = 6;
            $dtVencimento = date("d/m/Y", strtotime("+1 month", mktime(0, 0, 0, date('m'), 8, date('Y'))));

            for ($p = 0; $p < $nParcelas; $p++) {
                $vencimento = $this->getVencimento($p, $dtVencimento);
                // Montar array que será gravado no banco
                $dadosBoleto = [
                    'valor' => $valor++,
                    'vencimento' => $vencimento,
                    'usuarios_id' => $usu_id,
                    'vencimento' => date('Y-m-d H:i:s')
                ];

                $boletos_id[] = $this->Boleto_model->insertBoleto($dadosBoleto);
            }

            // Gerar boletos
            $this->genereteBoleto($boletos_id);
            $this->session->set_flashdata("success", "Boletos gerados com sucesso!");
            redirect('filiados');
        } else {
            $this->session->set_flashdata("success", "Boletos do usuário já foram gerados!");
            redirect('filiados');
        }
    }

    public function genereteBoleto($ids) {
        if (is_array($ids)) {
            // Buscar os boletos
            $boletos = $this->Boleto_model->findAllId($ids);
        } else {
            // Buscar o boleto
            $boletos[] = $this->Boleto_model->findId($ids);
        }

        foreach ($boletos as $boleto) {
            $beneficiario = $this->getBeneficiario();

            $pagador = $this->getPagador(
                    $boleto['pf_cpf'],
                    $boleto['pf_nome'],
                    $boleto['pf_cep'], $boleto['pf_endereco'], $boleto['pf_bairro'], $boleto['pf_uf'], $boleto['pf_cidade']);

            $bancoob[] = $this->getBoleto($boleto['bol_id'], $boleto['vencimento'], $boleto['valor'], $pagador, $beneficiario, $this->demonstrativo, $this->instrucoes);
            //debug($bancoob);
        }
        $this->generetePDF($bancoob, $boleto['usuario_email']);
    }

    /**
     * @return \Eduardokum\LaravelBoleto\Pessoa
     * @throws Exception
     */
    private function getBeneficiario() {
        $beneficiario = new \Eduardokum\LaravelBoleto\Pessoa;
        $beneficiario->setDocumento($this->cnpj)
                ->setNome($this->empresa)
                ->setCep($this->cep)
                ->setEndereco($this->empresa)
                ->setBairro($this->bairro)
                ->setUf($this->uf)
                ->setCidade($this->city);
        return $beneficiario;
    }

    /**
     * @param $vencimento
     * @param $valor
     * @param \Eduardokum\LaravelBoleto\Pessoa $pagador
     * @param \Eduardokum\LaravelBoleto\Pessoa $beneficiario
     * @param $demonstrativo
     * @param $instrucoes
     * @return \Eduardokum\LaravelBoleto\Boleto\Banco\Bancoob
     * @throws Exception
     */
    private function getBoleto($boletoId, $vencimento, $valor, \Eduardokum\LaravelBoleto\Pessoa $pagador, \Eduardokum\LaravelBoleto\Pessoa $beneficiario, $demonstrativo, $instrucoes) {
        $bancoob = new Eduardokum\LaravelBoleto\Boleto\Banco\Bancoob;
        $bancoob->setLogo('public/images/logo-nacao-fla-01.png')
                ->setDataVencimento(new \Carbon\Carbon($vencimento))
                ->setValor($valor)
                ->setNumero($boletoId)
                ->setNumeroDocumento($boletoId)
                ->setPagador($pagador)
                ->setBeneficiario($beneficiario)
                ->setCarteira(1)
                ->setAgencia(4001)
                ->setConvenio(568074)
                ->setConta(120838)
                ->setDescricaoDemonstrativo($demonstrativo)
                ->setInstrucoes($instrucoes);
        return $bancoob;
    }

    /**
     * @param $bancoob
     * @throws Exception
     */
    private function generetePDF($boletos, $email) {
        $nome = str_replace(' ', '_', $boletos[0]->pagador->nome);
        $pdf = new Eduardokum\LaravelBoleto\Boleto\Render\Pdf();
        //header("Content-type:application/pdf");
        $pdf->addBoletos($boletos);
        $pdf->showPrint();
        $pdf->hideInstrucoes();
        //$pdf->gerarBoleto($dest = Eduardokum\LaravelBoleto\Boleto\Render\Pdf::OUTPUT_STANDARD, $save_path = null);
        $pdf->gerarBoleto($dest = Eduardokum\LaravelBoleto\Boleto\Render\Pdf::OUTPUT_SAVE, $save_path = "public/anexos/{$nome}.pdf");

        $this->enviaBoletoEmail($email, $nome);
    }

    /**
     * @param $documento
     * @param $nome
     * @param $cep
     * @param $endereco
     * @param $bairro
     * @param $uf
     * @param $cidade
     * @return \Eduardokum\LaravelBoleto\Pessoa
     * @throws Exception
     */
    private function getPagador($documento, $nome, $cep, $endereco, $bairro, $uf, $cidade) {
        $pagador = new \Eduardokum\LaravelBoleto\Pessoa;
        $pagador->setDocumento($documento)
                ->setNome($nome)
                ->setCep($cep)
                ->setEndereco($endereco)
                ->setBairro($bairro)
                ->setUf($uf)
                ->setCidade($cidade);
        return $pagador;
    }

    /**
     * @param $mes
     * @param $dia
     * @param $ano
     * @return false|float
     */
    private function getIdade($mes, $dia, $ano) {
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
        return $idade;
    }

    private function getVencimento($nParcela, $dataUltimoVencimento = '') {
        if (!empty($dataUltimoVencimento)) {
            $dt = explode("/", $dataUltimoVencimento);
            $dia = $dt[0];
            $mes = $dt[1];
            $ano = $dt[2];
        } else {
            $dia = date("d");
            $mes = date("m");
            $ano = date("Y");
        }

        return date("Y-m-d", strtotime("+$nParcela month, +0 days", mktime(0, 0, 0, $mes, $dia, $ano)));
    }

    function enviaBoletoEmail($email = '', $nome = '') {
        //Cria e envia o e-mail        
        $nomeEmail = str_replace('_', ' ', $nome);
        $this->load->library('email');
        $config = array(
            'protocol' => 'sendmail',
            'charset' => 'utf-8',
            'newline' => '\r\n',
            'mailtype' => 'html',
        );
        $this->email->initialize($config);
        //$this->email->from('contato@torcedoresdanacaofla.org.br', 'Nação Fla');
        $this->email->from('contato@torcedoresdanacaofla.org.br', 'Nação Fla');
        $this->email->to($email);
        $this->email->bcc('torcedoresnacaofla@gmail.com');
        //$this->email->bcc('victor.hugo@codigosesolucoes.com.br');
        $nacao = 'Nação Fla';
        //$this->email->bcc('indiqueumamigonacaofla@gmail.com');
        $this->email->subject('Boletos de Filiação ' . $nacao);
        $logo = base_url() . 'public/images/logo-nacao-fla-01.png';
        $html_email = ("               
                <div class='container' style='text-align:center; padding:30px'>
                     <h1 class='text-center'><img src='{$logo}' style='opacity: .8; width: 500px; margin-bottom: -50px'></h1></div>
                    <div class='text-uppercase' style='text-align:center'><h2> Seja bem vindo(a) {$nomeEmail}!</h2></div>
                    <div class='container'>
                        <p style='text-align:center; padding: 0px 200px; text-align: justify; font-size:20px'>Prezado(a),</p>  
                        <p style='text-align:center; padding: 10px 200px; text-align: justify; font-size:18px'>
                        Seus boletos de filiação seguem em anexo!
                        </p>
                        <p>
                        O vencimento é sempre no dia 08 de cada mês.
                        </p>
                        <p>
                        Os boletos com vencimentos dentro do mês corrente serão registrados junto ao banco sempre no primeiro dia útil de cada mês.                         
                        </p>  
                        <p>
                        O pagamento do boleto só é aceito pela rede bancária após o registro do boleto junto ao banco.
                        </p>
                    </div>
                    <address style='text-align:center; background-color: #e83742; padding: 20px'>
                    <strong style='color: #fff'>Acesse nosso site</strong>
                    <address>
                        <strong><a href='https://www.torcedoresdanacaofla.org.br/' style='text-align:center; color: #000'>www.torcedoresdanacaofla.org.br</a></strong><br>
                    </address>
                </div>");
        $this->email->message($html_email);
        $this->email->attach("public/anexos/{$nome}.pdf");
        $this->email->send();
    }

}
